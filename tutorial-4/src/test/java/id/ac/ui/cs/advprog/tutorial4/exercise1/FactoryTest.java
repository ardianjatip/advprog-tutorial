package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.*;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.*;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.*;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.*;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.*;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Before;
import org.junit.Test;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class FactoryTest {

    private Cheese[] Keju;
    private Clams[] Kerang;
    private Dough[] Adonan;
    private Sauce[] Saus;
    private Veggies[] Veggiess;
    private Pizza pizza;

    @Before
    public void setUp() {
        //Cheese
        Keju = new Cheese[4];
        Keju[0] = new MozzarellaCheese();
        Keju[1] = new ParmesanCheese();
        Keju[2] = new ReggianoCheese();
        Keju[3] = new CheddarCheese();

        //Clam
        Kerang = new Clams[3];
        Kerang[0] = new FreshClams();
        Kerang[1] = new FrozenClams();
        Kerang[2] = new VenusClams();

        //Dough
        Adonan = new Dough[3];
        Adonan[0] = new ThickCrustDough();
        Adonan[1] = new ThinCrustDough();
        Adonan[2] = new EggCrustDough();

        //Sauce
        Saus = new Sauce[3];
        Saus[0] = new PlumTomatoSauce();
        Saus[1] = new MarinaraSauce();
        Saus[2] = new TartarSauce();

        //Veggies
        Veggiess = new Veggies[8];
        Veggiess[0] = new BlackOlives();
        Veggiess[1] = new Eggplant();
        Veggiess[2] = new Garlic();
        Veggiess[3] = new Mushroom();
        Veggiess[4] = new Onion();
        Veggiess[5] = new RedPepper();
        Veggiess[6] = new Spinach();
        Veggiess[7] = new Jalapeno();

    }

    @Test
    public void testToString() {
        //CheeseString
        assertEquals("Shredded Mozzarella", Keju[0].toString());
        assertEquals("Shredded Parmesan", Keju[1].toString());
        assertEquals("Reggiano Cheese", Keju[2].toString());
        assertEquals("Cheddar Cheese", Keju[3].toString());

        //Clam
        assertEquals("Fresh Clams from Long Island Sound", Kerang[0].toString());
        assertEquals("Frozen Clams from Chesapeake Bay", Kerang[1].toString());
        assertEquals("Venus Clams from Planet Venus", Kerang[2].toString());

        //Dough
        assertEquals("ThickCrust style extra thick crust dough", Adonan[0].toString());
        assertEquals("Thin Crust Dough", Adonan[1].toString());
        assertEquals("Egg Crust Dough", Adonan[2].toString());

        //Sauce
        assertEquals("Tomato sauce with plum tomatoes", Saus[0].toString());
        assertEquals("Marinara Sauce", Saus[1].toString());
        assertEquals("Tartar Sauce", Saus[2].toString());

        //Veggies
        assertEquals("Black Olives", Veggiess[0].toString());
        assertEquals("Eggplant", Veggiess[1].toString());
        assertEquals("Garlic", Veggiess[2].toString());
        assertEquals("Mushrooms", Veggiess[3].toString());
        assertEquals("Onion", Veggiess[4].toString());
        assertEquals("Red Pepper", Veggiess[5].toString());
        assertEquals("Spinach", Veggiess[6].toString());
        assertEquals("Jalapeno", Veggiess[7].toString());

    }
}
