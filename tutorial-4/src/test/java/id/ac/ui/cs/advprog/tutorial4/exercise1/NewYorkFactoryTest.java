package id.ac.ui.cs.advprog.tutorial4.exercise1;


import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Test;

import static org.junit.Assert.*;

public class NewYorkFactoryTest {

    @Test
    public void testCreatePizza() {
        PizzaStore nyStore = new NewYorkPizzaStore();

        Pizza nyPizza = nyStore.orderPizza("cheese");
        assertNotNull(nyPizza);
        assertEquals("New York Style Cheese Pizza", nyPizza.getName());
        nyPizza = nyStore.orderPizza("clam");
        assertNotNull(nyPizza);
        assertEquals("New York Style Clam Pizza", nyPizza.getName());
        nyPizza = nyStore.orderPizza("veggie");
        assertNotNull(nyPizza);
        assertEquals("New York Style Veggie Pizza", nyPizza.getName());

    }
}
