package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Test;

import static org.junit.Assert.*;

public class DepokFactoryTest {

    @Test
    public void testCanCreatePizza() {
        PizzaStore depokStore = new DepokPizzaStore();

        Pizza depokPizza = depokStore.orderPizza("cheese");
        assertEquals("---- Depok Style Cheese Pizza ----\n" +
                "Egg Crust Dough\n" +
                "Tartar Sauce\n" +
                "Cheddar Cheese\n", depokPizza.toString());

        depokPizza = depokStore.orderPizza("clam");
        assertNotNull(depokPizza);
        assertEquals("---- Depok Style Clam Pizza ----\n" +
                "Egg Crust Dough\n" +
                "Tartar Sauce\n" +
                "Cheddar Cheese\n" +
                "Venus Clams from Planet Venus\n", depokPizza.toString());

        depokPizza = depokStore.orderPizza("veggie");
        assertNotNull(depokPizza);
        assertEquals("---- Depok Style Veggie Pizza ----\n" +
                "Egg Crust Dough\n" +
                "Tartar Sauce\n" +
                "Cheddar Cheese\n" +
                "Spinach, Black Olives, Jalapeno, Eggplant\n", depokPizza.toString());
    }
}
