package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class EggCrustDough implements Dough {
    public String toString() {
        return "Egg Crust Dough";
    }
}
