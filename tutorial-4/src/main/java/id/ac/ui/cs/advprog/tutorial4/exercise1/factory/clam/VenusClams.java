package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class VenusClams implements Clams {

    public String toString() {
        return "Venus Clams from Planet Venus";
    }
}
